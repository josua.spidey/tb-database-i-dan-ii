I. Syarat membuat KK
1. Fotcopy KTP suami istri
2. Fotocopy buku nikah
3. Fotocopy akte kelahiran anak
4. Surat pengantar RT
5. Formulir permohonan KK baru

   Prosedur / Langkah-langkah pembuatan Kartu Kelurga(KK) baru :
* Pemohon datang ke Kantor Kelurahan setempat dengan membawa surat pengantar RT/RW dan berkas persyaratan yang telah ditentukan beserta dokumen aslinya;
* Petugas Kelurahan mengecek berkas yang bersangkutan dan memberikan blanko/ data isian KK serta memberikan informasi tentang persyaratan masa berlaku dan mekanisme pengisian blanko;
* Pemohon mengisi blanko/ data isian KK yang telah disediakan di Kelurahan masing-masing sesuai dengan wilayah tempat tinggalnya;
* Formulir yang sudah di isi diserahkan ke Kelurahan;
* Petugas Seksi Pemerintahan pada Kelurahan memeriksa dan meneliti blanko/ data isian KK dan meregister dalam buku Harian Peristiwa Kependudukan serta mengajukan kepada Lurah/Kepala Desa untuk ditandatangani;
* Apabila berkas belum lengkap maka petugas mengembalikan kepada pemohon untuk dilengkapi;
* Setelah berkas ditandatangani Lurah/Kepala Desa, Petugas Seksi Pemerintahan pada Kelurahan mencatat dalam Buku Induk Penduduk dan menyerahkan kembali kepada pemohon beserta dokumen aslinya;
* Pemohon mendatangi loket pelayanan KK dan KTP yang ada di Kantor Kecamatan dengan membawa berkas lengkap beserta dokumen asli;
* Petugas Pelayanan KK dan KTP yang ada di Kecamatan menerima dan memverifikasi berkas serta mencatat data pemohon dalam Buku Permohonan KK.
* Petugas Pelayanan KK dan KTP meregister berkas permohanan dan menerbitkan tanda terima pendaftaran;
* berkas permohonan yang telah diregister dan berkas lainnya diteruskan ke Operator komputer;
* Operator komputer menerima dan mengecek biodata penduduk pada berkas permohonan dengan mensinkronisasi biodata yang diterima ke dalam Aplikasi SIAK, data yang tidak valid dikembalikan kepada petugas loket;
* Operator Komputer mencetak KK sesuai data yang valid pada blangko asli rangkap 5 (lima), serta mencatat nomor serial blanko yang telah diterbitkan;
* Operator Komputer menyerahkan Cetakan KK ke petugas Verifikasi Dinas Kependudukan dan Pencatatan Sipil;
* KK diserahakan ke bidang Pendaftaran penduduk untuk diteliti dan diparaf Kepala Bidang Pendaftaran Penduduk untuk kemudian diteruskan ke kepala Dinas untuk ditandatangani dan distempel basah selanjutnya diserahkan kembali kepada staf bidang pendaftaran penduduk untuk diteruskan ke bagian loket pengambilan ;
* Pemohon pada batas waktu proses yang telah ditentukan mendatangi loket Pelayanan KK dan KTP di Dinas Kependudukan dan Pencatatan Sipil;
II. Syarat Pembuatan e-KTP

1. Berusia 17 tahun
2. Fotocopy KK
3. Fotocopy akta kelahiran 
4. Surat pengantar RT/RW


Prosedur /langkah-langkah pembuatan E-ktp :
1. Datang ke RT/RW untuk membuat surat pengantar dari RT/RW
2. Lengkapi persyaratan yang harus dibawa atau disiapkan.
3. Bawa persyaratan yang telah disiapkan ke kelurahan,lalu mengisi formulir yang telah diberikan petugas.
4. Kemudian bawa formulir  beserta persyaratan yang lain ke distrik capil.
5. Ambil nomor antrian ,Tunggu nomor di panggil.
6. Petugas akan melakukan pengambilan foto diri ditempat ,pengambilan tanda tangan ,selanjutnya perekaman data sidik jari ,scan retina mata.
7. Lalu petugas akan memberikan surat pemanggilan ,yang sudah ditanda tangani dan distempel petugas.
8. setelah selesai semua langkahnya ,tunggu proses pencetakan e-ktp biasanya memerlukan waktu kurang lebih 2 minggu


III. Syarat Pembutan KK dalam kasus Poligami
1. Surat permohonan Izin Poligami;
- Dibuat sebanyak 8 (Delapan)
- Siapkan Softfile (Flasdisk/CD)
2. Foto Copy KTP Suami dan Istri + Kartu Keluarga;
3. Foto Copy Buku/ Akta Nikah Pemohon;
4. Surat Pernyataan / Surat Keterangan Penghasilan;
5. Daftar Harta Kekayaan;
6. Surat Pernyataan Berlaku Adil;
7. Surat Pernyataan Bersedia Dimadu;
8. Surat Pernyataan Bersedia menjadi istri kedua;

